import unittest
from app import message_length


class Test(unittest.TestCase):
    def test_length(self):
        self.assertEqual(message_length(), '8')


if __name__ == "__main__":
    unittest.main()
